import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
} from '@angular/material/paginator';
import { getPtBrPaginatorIntl } from 'src/app/core/intl/pt-br-paginatior-intl';
import { BookService } from 'src/app/core/services/book.service';
import { CardBookModule } from 'src/app/shared/card-book/card-book.module';
import { HeaderModule } from 'src/app/shared/header/header.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HeaderModule,
    CardBookModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
  ],
  providers: [
    BookService,
    { provide: MatPaginatorIntl, useValue: getPtBrPaginatorIntl() },
  ],
})
export class HomeModule {}
