import { Component } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { finalize } from 'rxjs/operators';
import { Book } from 'src/app/core/models/book';
import { Paginate } from 'src/app/core/models/paginate';
import { BookService } from 'src/app/core/services/book.service';
import { LoadingService } from 'src/app/core/services/loading.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  bookPaginate: Paginate<Book> = {
    data: [],
    page: 1,
    totalPages: 0,
    totalItems: 0,
  };

  amount = '12';
  page = '1';

  constructor(
    private bookService: BookService,
    private loadingService: LoadingService,
  ) {
    this.getBooks(this.page, this.amount);
  }

  getBooks(page: string, amount: string): void {
    this.loadingService.show('Carregando livros...');
    this.bookService
      .getBooks(page, amount)
      .pipe(
        finalize(() => {
          this.loadingService.hide();
        }),
      )
      .subscribe({
        next: (bookPaginate) => {
          this.bookPaginate = bookPaginate;
        },
      });
  }

  onPageChange(page: PageEvent): void {
    this.page = String(page.pageIndex + 1);
    this.amount = String(page.pageSize);
    this.getBooks(this.page, this.amount);
  }
}
