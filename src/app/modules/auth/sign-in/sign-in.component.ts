import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTooltip } from '@angular/material/tooltip';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoadingService } from 'src/app/core/services/loading.service';
import { TokenService } from 'src/app/core/services/token.service';
import { FormBaseComponent } from 'src/app/shared/base/form-base.component';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent
  extends FormBaseComponent
  implements OnInit, AfterViewInit
{
  @ViewChild('tooltip') tooltip!: MatTooltip;
  @ViewChildren(FormControlName, { read: ElementRef })
  formInputElements!: ElementRef[];

  form!: FormGroup;
  credentialsInvalid: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private tokenService: TokenService,
    private loadingService: LoadingService,
    readonly snackBar: MatSnackBar,
  ) {
    super();

    this.validationMessages = {
      email: {
        required: 'O email é obrigatorio.',
      },
      password: {
        required: 'A senha é obrigatorio.',
      },
    };

    super.configureMessagesValidationBase(this.validationMessages);
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  ngAfterViewInit(): void {
    super.configureFormValidationBase(this.formInputElements, this.form);
    this.tooltip.disabled = true;
  }

  removeInvalidLoginErrors(formGroup: FormGroup): void {
    if (this.credentialsInvalid) {
      this.tooltip.disabled = true;
      Object.values(formGroup.controls).forEach((control: AbstractControl) => {
        if (control.errors?.['invalidLogin']) {
          delete control.errors['invalidLogin'];
          control.updateValueAndValidity();
        }
      });
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      const { email, password } = this.form.value;

      this.loadingService.show();
      this.authService.signIn(email, password).subscribe({
        next: (response) => {
          const authorization = response.headers.get('Authorization');
          const refreshToken = response.headers.get('Refresh-Token');
          const user = response.body;

          if (authorization) {
            this.tokenService.setToken(authorization);
          }

          if (refreshToken) {
            this.tokenService.setRefreshToken(refreshToken);
          }

          if (user) {
            this.tokenService.setUserToken(user);
          }

          this.router.navigate(['/home']);
        },
        error: () => {
          this.credentialsInvalid = true;
          this.form.get('email')?.setErrors({ invalidLogin: true });
          this.form.get('password')?.setErrors({ invalidLogin: true });
          this.tooltip.disabled = false;
          this.tooltip.show();
        },
        complete: () => {
          this.loadingService.hide();
        },
      });
    }
  }
}
