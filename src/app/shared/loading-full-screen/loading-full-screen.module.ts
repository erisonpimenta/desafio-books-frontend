import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoadingFullScreenComponent } from './loading-full-screen.component';

@NgModule({
  declarations: [LoadingFullScreenComponent],
  imports: [CommonModule, MatProgressSpinnerModule],
  exports: [LoadingFullScreenComponent],
})
export class LoadingFullScreenModule {}
