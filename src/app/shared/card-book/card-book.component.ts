import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Book } from 'src/app/core/models/book';
import { BookService } from 'src/app/core/services/book.service';
import { LoadingService } from 'src/app/core/services/loading.service';
import { DialogDetailsBookComponent } from '../dialog-details-book/dialog-details-book.component';

@Component({
  selector: 'app-card-book',
  templateUrl: './card-book.component.html',
  styleUrls: ['./card-book.component.scss'],
})
export class CardBookComponent {
  @Input() book!: Book;

  constructor(
    private dialog: MatDialog,
    private bookService: BookService,
    private loadingService: LoadingService,
  ) {}

  openBookDetails(book: Book): void {
    this.loadingService.show();
    this.bookService.getBook(book.id).subscribe({
      next: (book) => {
        this.dialog.open(DialogDetailsBookComponent, {
          data: book,
          panelClass: 'dialog-details-book',
        });
      },
      complete: () => {
        this.loadingService.hide();
      },
    });
  }
}
