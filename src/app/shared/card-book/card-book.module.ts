import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogDetailsBookModule } from '../dialog-details-book/dialog-details-book.module';
import { CardBookComponent } from './card-book.component';

@NgModule({
  declarations: [CardBookComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatDialogModule,
    DialogDetailsBookModule,
  ],
  exports: [CardBookComponent],
  providers: [],
})
export class CardBookModule {}
