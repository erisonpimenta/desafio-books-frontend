import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDetailsBookComponent } from './dialog-details-book.component';

describe('DialogDetailsBookComponent', () => {
  let component: DialogDetailsBookComponent;
  let fixture: ComponentFixture<DialogDetailsBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogDetailsBookComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDetailsBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
