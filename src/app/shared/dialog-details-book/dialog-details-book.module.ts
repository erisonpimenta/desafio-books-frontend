import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { DialogDetailsBookComponent } from './dialog-details-book.component';

@NgModule({
  declarations: [DialogDetailsBookComponent],
  imports: [CommonModule, MatDialogModule, MatIconModule],
  exports: [DialogDetailsBookComponent],
  providers: [],
})
export class DialogDetailsBookModule {}
