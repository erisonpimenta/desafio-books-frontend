import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Book } from 'src/app/core/models/book';

@Component({
  templateUrl: './dialog-details-book.component.html',
  styleUrls: ['./dialog-details-book.component.scss'],
})
export class DialogDetailsBookComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public book: Book,
    public dialogRef: MatDialogRef<DialogDetailsBookComponent>,
  ) {}

  onClose(): void {
    this.dialogRef.close();
  }
}
