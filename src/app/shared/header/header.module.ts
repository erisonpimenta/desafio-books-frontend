import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { LogoModule } from '../logo/logo.module';
import { HeaderComponent } from './header.component';

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, MatIconModule, LogoModule],
  exports: [HeaderComponent],
  providers: [],
})
export class HeaderModule {}
