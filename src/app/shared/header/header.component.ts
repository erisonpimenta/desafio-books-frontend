import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Gender, User } from 'src/app/core/models/user';
import { AuthService } from 'src/app/core/services/auth.service';
import { TokenService } from 'src/app/core/services/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  user: User | null;

  constructor(
    private tokenService: TokenService,
    private authService: AuthService,
    private router: Router,
  ) {
    this.user = this.tokenService.getUserToken();
  }

  welcomeText(gender: Gender): string {
    if (gender === Gender.Male) {
      return 'Bem vindo,';
    }
    return 'Bem vinda,';
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/auth']);
  }
}
