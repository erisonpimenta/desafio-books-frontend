import { ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { fromEvent, merge, Observable } from 'rxjs';
import {
  DisplayMessage,
  GenericValidator,
  ValidationMessages,
} from '../../core/utils/generic-form-validation';

export abstract class FormBaseComponent {
  displayMessage: DisplayMessage = {};
  genericValidator!: GenericValidator;
  validationMessages!: ValidationMessages;

  unsavedChanges!: boolean;

  protected configureMessagesValidationBase(
    validationMessages: ValidationMessages,
  ) {
    this.genericValidator = new GenericValidator(validationMessages);
  }

  protected configureFormValidationBase(
    formInputElements: ElementRef[],
    formGroup: FormGroup,
  ) {
    const controlBlurs: Observable<any>[] = formInputElements.map(
      (formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'),
    );

    merge(...controlBlurs).subscribe(() => {
      this.validateForm(formGroup);
    });
  }

  protected validateForm(formGroup: FormGroup) {
    this.displayMessage = this.genericValidator.processMessages(formGroup);
    this.unsavedChanges = true;
  }
}
