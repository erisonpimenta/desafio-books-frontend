import { Component } from '@angular/core';
import { LoadingService } from './core/services/loading.service';

@Component({
  selector: 'app-root',
  template: `
    <app-loading-full-screen
      [show]="loading"
      [text]="textLoading"
    ></app-loading-full-screen>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  title = 'ioasys-books';
  loading = false;
  textLoading = '';

  constructor(private loadingService: LoadingService) {
    this.loadingService.loadingEvent$.subscribe((item: string) => {
      if (item) {
        this.loading = true;
        this.textLoading = item;
      } else {
        this.loading = false;
        this.textLoading = '';
      }
    });
  }
}
