import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { TokenService } from '../services/token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanLoad {
  constructor(private router: Router, private tokenService: TokenService) {}

  canLoad(): boolean {
    if (!this.tokenService.getToken()) {
      this.router.navigate(['/auth']);
      return false;
    }
    return true;
  }
}
