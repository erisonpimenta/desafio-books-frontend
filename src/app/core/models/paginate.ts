export interface Paginate<T> {
  data: T[];
  page: number;
  totalPages: number;
  totalItems: number;
}

export interface PaginateOptions {
  page: number;
  amount: number;
}
