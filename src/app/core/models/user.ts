export interface User {
  id: string;
  name: string;
  birthdate: string;
  gender: Gender;
}

export enum Gender {
  Male = 'M',
  Female = 'F',
}
