import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TokenService } from '../services/token.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router, private tokenService: TokenService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((response: Response | any) => {
        let customError: string[] = [];
        if (response instanceof HttpErrorResponse) {
          if (response.status === 403) {
            this.router.navigate(['/access-denied']);
          }

          if (response.statusText === 'Unknown Error') {
            customError.push('Ocorreu um erro desconhecido');
            response.error.errors = customError;
          }

          if (response.status === 500) {
            customError.push('Ocorreu um erro interno no servidor');
            response.error.errors = customError;
          }
        }

        return throwError(response);
      }),
    );
  }
}
