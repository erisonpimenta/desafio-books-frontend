import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { TokenService } from '../services/token.service';

@Injectable()
export class OauthInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private tokenService: TokenService,
    private authService: AuthService,
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): any {
    const token = this.tokenService.getToken();
    const refreshToken = this.tokenService.getRefreshToken();

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token,
        },
      });
    }

    if (!request.headers.has('Content-Type')) {
      request = request.clone({
        setHeaders: {
          'content-type': 'application/json',
        },
      });
    }

    request = request.clone({
      headers: request.headers.set('Accept', 'application/json'),
    });

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401 && token && refreshToken) {
          this.authService.refreshToken(refreshToken).subscribe(
            (response) => {
              const authorization = response.headers.get('Authorization');
              const refreshToken = response.headers.get('Refresh-Token');

              if (authorization) {
                this.tokenService.setToken(authorization);
              }

              if (refreshToken) {
                this.tokenService.setRefreshToken(refreshToken);
              }

              location.reload();
            },
            () => {
              this.tokenService.removeToken();
              this.tokenService.removeRefreshToken();
              this.router.navigate(['/auth/sign-in'], {
                queryParams: { returnUrl: this.router.url },
              });
            },
          );
        }
        return throwError(error);
      }),
    );
  }
}
