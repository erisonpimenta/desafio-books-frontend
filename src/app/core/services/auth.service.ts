import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from 'src/app/core/services/base.service';
import { User } from '../models/user';
import { TokenService } from './token.service';

@Injectable()
export class AuthService extends BaseService {
  constructor(private http: HttpClient, private tokenService: TokenService) {
    super();
  }

  signIn(email: string, password: string): Observable<HttpResponse<User>> {
    return this.http.post<User>(
      `${this.url}/auth/sign-in`,
      {
        email,
        password,
      },
      {
        observe: 'response',
      },
    );
  }

  logout(): void {
    this.tokenService.removeToken();
    this.tokenService.removeRefreshToken();
  }

  refreshToken(refreshToken: string): Observable<HttpResponse<void>> {
    return this.http.post<void>(
      `${this.url}/auth/refresh-token`,
      {
        refreshToken,
      },
      { observe: 'response' },
    );
  }
}
