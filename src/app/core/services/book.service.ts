import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from '../models/book';
import { Paginate } from '../models/paginate';
import { BaseService } from './base.service';

@Injectable()
export class BookService extends BaseService {
  constructor(private http: HttpClient) {
    super();
  }

  getBooks(
    page = '1',
    amount = '10',
    tittle?: string,
    category?: string,
  ): Observable<Paginate<Book>> {
    const params = new HttpParams({ fromObject: { page, amount } });

    if (tittle) {
      params.set('tittle', tittle);
    }

    if (category) {
      params.set('category', category);
    }

    return this.http.get<Paginate<Book>>(`${this.url}/books`, { params });
  }

  getBook(id: string): Observable<Book> {
    return this.http.get<Book>(`${this.url}/books/${id}`);
  }
}
