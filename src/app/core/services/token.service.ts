import { Injectable } from '@angular/core';
import { User } from '../models/user';

const ACCESS_TOKEN = 'access_token';
const REFRESH_TOKEN = 'refresh_token';
const USER_TOKEN = 'user_token';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor() {}

  getToken(): string | null {
    return localStorage.getItem(ACCESS_TOKEN);
  }

  getRefreshToken(): string | null {
    return localStorage.getItem(REFRESH_TOKEN);
  }

  getUserToken(): User | null {
    const userToken = localStorage.getItem(USER_TOKEN);
    if (userToken) {
      return JSON.parse(userToken);
    }
    return null;
  }

  setToken(token: string): void {
    localStorage.setItem(ACCESS_TOKEN, token);
  }

  setRefreshToken(refreshToken: string): void {
    localStorage.setItem(REFRESH_TOKEN, refreshToken);
  }

  setUserToken(userToken: User): void {
    localStorage.setItem(USER_TOKEN, JSON.stringify(userToken));
  }

  removeToken(): void {
    localStorage.removeItem(ACCESS_TOKEN);
  }

  removeRefreshToken(): void {
    localStorage.removeItem(REFRESH_TOKEN);
  }

  removeUserToken(): void {
    localStorage.removeItem(USER_TOKEN);
  }
}
