import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export abstract class BaseService {
  readonly url: string = '';

  constructor() {
    this.url = environment.baseURL;
  }
}
